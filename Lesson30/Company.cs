﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson30
{
    public class Company
    {
        private List<Worker> workers; // null

        public Company()
        {
            workers = new List<Worker>(); // 0
        }

        public void Add(Worker worker)
        {
            workers.Add(worker);
        }

        public Worker SearchWorkerById(int id)
        {
            return workers.FirstOrDefault(w => w.Id == id);
        }

        public int GetExpirienceByWorkerId(int workerId)
        {
            var worker = SearchWorkerById(workerId);

            if (worker == null)
                throw new Exception($"Не удалось найти работника по id: {workerId}");

            return worker.GetExperience();
        }

        public void PrintInfo()
        {
            foreach (var worker in workers)
                worker.Info();
        }

        public void User()
        {

        }
    }
}
