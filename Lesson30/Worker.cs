﻿using System;

namespace Lesson30
{
    public class Worker
    {
        public Worker()
        {
            ValidateFields();
        }
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int WorkStartYear { get; set; }

        /// <summary>
        /// Получение стажа работы
        /// </summary>
        /// <returns></returns>
        public int GetExperience() => DateTime.Now.Year - WorkStartYear;

        /// <summary>
        /// Информация о работнике
        /// </summary>
        public void Info()
        {
            Console.WriteLine($"FullName: {FullName} DateOfBirth: {DateOfBirth.ToShortDateString()} WorkStartYear: {WorkStartYear}");
        }

        private void ValidateFields()
        {
            if (string.IsNullOrEmpty(FullName))
                throw new Exception("ФИО не может быть пустым");

            if (!string.IsNullOrEmpty(FullName))
            {
                for (int i = 0; i < FullName.Length; i++)
                {
                    // проверка на существование цифр
                }
            }



            if (DateOfBirth.Year < DateTime.Now.Year - 10)
                throw new Exception("Работнику меньше 10 лет");

            if (WorkStartYear > DateTime.Now.Year)
                throw new Exception("Старт начала работы не может быть больше текущего года");
        }
    }
}
